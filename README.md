Groupshell

Este es un ejemplo de como utilizar la biblioteca de funciones y tomar
los ejemplos.

Requerimientos para compilar.

ANTLR Version 3.3
ANTLR Bibliotecas de C para ANTLR == 3.2.*
Compilador de gcc >= 4.6.3
GNU Make >= 3.82
readline >= 6 

Variables definidas para compilar

CLASSPATH=<Directorio intalado anltr>/antlr-3.3-complete.jar:<otros rutas para otros jars>

Variables definidas para ejecutar:

Si instalan la biblioteca de funciones esta deberá queda en el directorio 
(por omisión): /usr/local/lib, entonces definir:

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib

Manual de uso del analizador sintáctico para el lenguaje de comandos:




