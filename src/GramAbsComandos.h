#ifndef _GRAMABSCOMANDOS_H
#define _GRAMABSCOMANDOS_H

#include <antlr3string.h>
/*
 * Estructuras de datos:
 *
 */

typedef char* pCadena;

struct nodolista {
  void* elem;
  struct nodolista* sig;
};

typedef struct nodolista NodoLista;
typedef struct nodolista* pNodoLista;

struct cablista {
  pNodoLista cab;
  pNodoLista ult;
};

typedef struct cablista  Lista;
typedef struct cablista* pLista;

struct comandocrear {
  pCadena prog;
  pCadena arg;
};

typedef struct comandocrear  ComandoCrear;
typedef struct comandocrear* pComandoCrear;

struct comandodestruir {
  pLista listaProgs;
};

typedef struct comandodestruir  ComandoDestruir;
typedef struct comandodestruir* pComandoDestruir;

struct comandolistar {
  pLista listaIds;
};

typedef struct comandolistar  ComandoListar;
typedef struct comandolistar* pComandoListar;

struct comandoborrar {
  pLista listaPends;
};

typedef struct comandoborrar  ComandoBorrar;
typedef struct comandoborrar* pComandoBorrar;

struct comandoejecutar {
  pLista  listaPends;
  pCadena fen;
  pCadena fsal;
};

typedef struct comandoejecutar  ComandoEjecutar;
typedef struct comandoejecutar* pComandoEjecutar; 

struct comandoenviarsenal {
  pLista listaEventos;
};

typedef struct comandoenviarsenal ComandoEnviarSenal;
typedef struct comandoenviarsenal* pComandoEnviarSenal;

union comando {
  ComandoCrear       crear;
  ComandoDestruir    destruir;
  ComandoListar      listar;
  ComandoBorrar      borrar;
  ComandoEjecutar    ejecutar;
  ComandoEnviarSenal enviarsenal;
};

typedef union comando Comando;
typedef union comando pComando;


enum comandomarbete { CMD_CREAR,
		      CMD_DESTRUIR,
		      CMD_LISTAR,
		      CMD_BORRAR,
		      CMD_EJECUTAR,
		      CMD_ENVIARSENAL
};

typedef enum comandomarbete ComandoEtiqueta;

struct comandos {
  ComandoEtiqueta etiqueta;
  Comando cmd;
};

typedef struct comandos Comandos;
typedef struct comandos* pComandos;

enum ids { ID_PROG, ID_PEN };

typedef enum ids IDEtiqueta;

struct id {
  IDEtiqueta etiqueta;
  int id;
};

typedef struct id ID;
typedef struct id* pID;

struct evento {
  int evento;
  pLista listaProgs;
};

typedef struct evento Evento; 
typedef struct evento* pEvento;

/*
 * Definicion de funciones:
 */

pComandos nuevoComandoCrear(pCadena prog, pCadena arg);
pComandos nuevoComandoDestruir(pLista listaProgs);
pComandos nuevoComandoListar();
pComandos nuevoComandoEjecutar(pLista listaPends, pCadena fen, pCadena fsal);
pComandos nuevoComandoBorrar(pLista listaPends);
pComandos nuevoComandoEnviarSenal(pLista listaEventos);

pComandos anadirListaAComandoListar(pComandos, pLista listaIds);

void borrarComandoCrear(pComandos crear);
void borrarComandoDestruir(pComandos destruir);
void borrarComandoListar(pComandos listar);
void borrarComandoEjecutar(pComandos ejecutar);
void borrarComandoBorrar(pComandos borrar);
void borrarComandoEnviarSenal(pComandos enviarsenal);

pID nuevoID(IDEtiqueta etiqueta, int id);
pID nuevoIDPen(pANTLR3_STRING str);
pID nuevoIDProg(pANTLR3_STRING str);

pEvento nuevoEvento(int evento, pLista listaProg);

pCadena nuevaCadenaNB(int tam);
pCadena nuevaCadenaCopiaANTLR3Str(pANTLR3_STRING str);
pCadena nuevaCadenaCopiaStr(char *str);
void borrarCadena(pCadena pcadena);

pLista nuevaListaVacia();
pLista adicionarElemLista(pLista plista, void *elem);
#endif
