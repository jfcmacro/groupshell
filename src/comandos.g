grammar comandos;

options {
	language=C;
}

@header {
#include "GramAbsComandos.h"
#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>
}

@lexer::header {
#include "GramAbsComandos.h"
}

comandos returns [pComandos ret]
	:	cmd=comando EOF            { $ret = cmd; }
	;

comando returns [pComandos ret]
    :       cmd=comandocrear       { $ret = cmd; }
	|       cmd=comandodestruir    { $ret = cmd; }
	|       cmd=comandolistar      { $ret = cmd; }
	|       cmd=comandoejecutar    { $ret = cmd; }
	|       cmd=comandoborrar      { $ret = cmd; }
	|       cmd=comandoenviarsenal { $ret = cmd; }
	|
	;
catch[RecognitionException re]
   {
     fprintf(stderr, "Error de sintaxis: Comando desconocido\n");
     $ret = NULL;
   }
comandocrear returns [pComandos cmd]
	:	 'crear' nomprog=NOMPROG par=PARAMETERS
        {
            $cmd = nuevoComandoCrear(nuevaCadenaCopiaANTLR3Str($nomprog.text)
                                    ,nuevaCadenaCopiaANTLR3Str($par.text));
        }
	;
catch[RecognitionException re]
   {
     fprintf(stderr, "Error de sintaxis: Comando crear\n");
     $cmd = NULL;
   }

comandodestruir returns [pComandos cmd]
	:	'destruir' l=listaProgs
        { $cmd = nuevoComandoDestruir(l); }
	;
catch[RecognitionException re]
   {
     fprintf(stderr, "Error de sintaxis: Comando destruir\n");
     $cmd = NULL;
   }


comandolistar returns [pComandos cmd]
@init {
   $cmd = nuevoComandoListar();
}
	:	'listar'  (l=listaIds
            { 
                $cmd = anadirListaAComandoListar($cmd,l);
            }
        )?
	;
catch[RecognitionException re]
   {
     fprintf(stderr, "Error de sintaxis: Comando listar\n");
     $cmd = NULL;
   }

comandoejecutar returns [pComandos cmd]
	:	'ejecutar' l=listaPends
        '<-'  '(' fen=ficheroentrada  ',' fsal=ficherosalida ')'
        { $cmd = nuevoComandoEjecutar(l,fen,fsal); }
	;
catch[RecognitionException re]
   {
     fprintf(stderr, "Error de sintaxis: Comando ejecutar\n");
     $cmd = NULL;
   }

comandoborrar returns [pComandos cmd]
	:	'borrar' l=listaPends2        { $cmd = nuevoComandoBorrar(l); }
	;
catch[RecognitionException re]
   {
     fprintf(stderr, "Error de sintaxis: Comando borrar\n");
     $cmd = NULL;
   }

comandoenviarsenal returns [pComandos cmd]
	:	'evento'  l=listaEventos
        { $cmd = nuevoComandoEnviarSenal(l); }
	;
catch[RecognitionException re]
   {
     fprintf(stderr, "Error de sintaxis: Comando enviarsenal\n");
     $cmd = NULL;
   }

ficheroentrada returns [pCadena pcadena ]
	:	NOMPROG
        { $pcadena = nuevaCadenaCopiaANTLR3Str($NOMPROG.text); }
	;

ficherosalida returns [pCadena pcadena ]
	:	NOMPROG
        { $pcadena = nuevaCadenaCopiaANTLR3Str($NOMPROG.text); }
	;

listaProgs returns [pLista lista]
@init {
    $lista = nuevaListaVacia();
}
    : v=IDPROG {$lista = adicionarElemLista($lista,
                                            (void *) nuevoIDProg($v.text));
               }
        (','
      l=IDPROG {
               $lista = adicionarElemLista($lista,
                                           (void *) nuevoIDProg($l.text));
               })*
    ;

listaPends returns [pLista lista]
@init {
    $lista = nuevaListaVacia();
}
    : v=IDPEN {$lista = adicionarElemLista($lista,
                                           (void*) nuevoIDPen($v.text));
              }
        ('->' l=IDPEN
              {$lista = adicionarElemLista($lista,
                                           (void*) nuevoIDPen($l.text));
              })*
    ;

listaPends2 returns [pLista lista]
@init {
    $lista = nuevaListaVacia();
}
    : v=IDPEN {$lista = adicionarElemLista($lista,
                                           (void *) nuevoIDPen($v.text));
              }
        (',' l=IDPEN
              {$lista = adicionarElemLista($lista,
                                           (void*) nuevoIDPen($l.text));
              })*
    ;

listaIds returns [pLista lista]
@init {
    $lista = nuevaListaVacia();
}
    : v=ids { $lista = adicionarElemLista($lista, (void *) v); }
        (',' l=ids { $lista = adicionarElemLista($lista, (void*) l); })*
    ;

listaEventos returns [pLista lista]
@init {
    $lista = nuevaListaVacia();
}
    : (e=evento '{' l=listaProgs '}' 
        { $lista = adicionarElemLista($lista, (void*) nuevoEvento(e,l)); })+
    ;

evento returns [int nevento]
    : 'SIGHUP'          { $nevento = SIGHUP;  }
    | 'SIGINT'          { $nevento = SIGINT;  }
    | 'SIGQUIT'         { $nevento = SIGQUIT; }
    | 'SIGILL'          { $nevento = SIGILL;  }
    | 'SIGABRT'         { $nevento = SIGABRT; }
    | 'SIGFPE'          { $nevento = SIGFPE;  }
    | 'SIGKILL'         { $nevento = SIGKILL; }
    | 'SIGSEGV'         { $nevento = SIGSEGV; }
    | 'SIGPIPE'         { $nevento = SIGPIPE; }
    | 'SIGALRM'         { $nevento = SIGALRM; }
	| 'SIGTERM'         { $nevento = SIGTERM; }
    | 'SIGUSR1'         { $nevento = SIGUSR1; }
    | 'SIGUSR2'         { $nevento = SIGUSR2; }
    | 'SIGCHLD'         { $nevento = SIGCHLD; }
    | 'SIGCONT'         { $nevento = SIGCONT; }
    | 'SIGSTOP'         { $nevento = SIGSTOP; }
    | 'SIGTSTP'         { $nevento = SIGTSTP;  }
    | 'SIGTTIN'         { $nevento = SIGTTIN; }
    | 'SIGTTOU'         { $nevento = SIGTTOU; }
	;


ids  returns [pID pid]
    :  id=idpen                           { $pid = id; }
    |  id=idprog                          { $pid = id; }
    ;

idpen returns [pID pid]
    : IDPEN                            { $pid = nuevoIDPen($IDPEN.text);  }
    ;

idprog returns [pID pid]
    : IDPROG                           { $pid = nuevoIDProg($IDPROG.text); }
    ;

IDPEN
    : (('t')(INT))
	;

IDPROG
    : (('p')(INT))
	;

fragment ID
    :	('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    ;


DDOT
    :	'..'
	;


DOT
    :	 '.'
	;

NOMPROG
    :	('/')(ID)(('/')ID)*
	| ID
	| (DDOT|DOT)(('/')(ID))*
	;

INT
    : ('0') | ('1'..'9')('0'..'9')*
	;


WS
    :   ( ' '
        | '\t'
        | '\r'
        | '\n'
        ) {$channel=HIDDEN;}
    ;

PARAMETERS
	: '"' .* '"'
	;
