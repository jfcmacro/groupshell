/* Autor: Juan Francisco Cardona McCormick
 * Fecha: 25/04/2013
 *
 * Historia de modificaciones.
 *  01/05/2013 - Completando las clases.
 */


#include "comandosLexer.h"
#include "comandosParser.h"
#include <stdio.h>
#include <string.h>
#include <readline/readline.h>
#include <readline/history.h>

const char *prompt = ">>> ";

int
main(int argc, char *argv[]) {
  char *linea;
  pANTLR3_INPUT_STREAM input;
  pcomandosLexer lexer;
  pcomandosParser parser;
  pANTLR3_COMMON_TOKEN_STREAM tokens;
  pANTLR3_INPUT_STREAM datosALexer;
  uint32_t tamanoBuffer;
  pANTLR3_UINT8 nombreDatos = (pANTLR3_UINT8) "Entrada estandar";
  pComandos ret;

  while ((linea = readline(prompt))) {

    tamanoBuffer = strlen(linea);

    input = antlr3NewAsciiStringCopyStream(linea,
					   tamanoBuffer,
					   nombreDatos);
    lexer = comandosLexerNew(input);
    tokens = antlr3CommonTokenStreamSourceNew(ANTLR3_SIZE_HINT,
					      TOKENSOURCE(lexer));
    parser = comandosParserNew(tokens);

    ret = parser->comandos(parser);

    if (!ret) {
      fprintf(stderr, "Comando no reconocido\n");
    }
    else {
      switch(ret->etiqueta) {
      case CMD_CREAR:

	printf("exec(%s, %s);\n", ret->cmd.crear.prog,
	       ret->cmd.crear.arg);
	break;

      case CMD_DESTRUIR:
	{
	  pNodoLista cablista = ret->cmd.destruir.listaProgs->cab;

	  while (cablista) {
	    printf("t%d", ((pID) cablista->elem)->id);
	    cablista = cablista->sig;
	    if (cablista) printf(", ");
	  }
	  break;
	}

      case CMD_LISTAR:
	{
	  if (ret->cmd.listar.listaIds) {
	    pNodoLista cablista = ret->cmd.listar.listaIds->cab;

	    while (cablista) {
	      pID pid = (pID) cablista->elem;
	      switch(pid->etiqueta) {
	      case ID_PROG:
		printf("p");
		break;
	      case ID_PEN:
		printf("t");
		break;
	      default:
		printf("etiqueta: %d IDPROG %d IDPEN %d\n",
		       pid->etiqueta, IDPROG, IDPEN);
	      }
	      printf("%d", pid->id);
	      cablista = cablista->sig;
	      if (cablista) printf(", ");
	    }
	    printf("\n");
	  }
	  break;
	}

      case CMD_BORRAR:
	{
	  pNodoLista cablista = ret->cmd.borrar.listaPends->cab;

	  while (cablista) {
	    printf("t%d", ((pID) cablista->elem)->id);
	    cablista = cablista->sig;
	    if (cablista) printf(", ");
	  }
	  break;
	}

      case CMD_EJECUTAR:
	{
	  pNodoLista cablista = ret->cmd.ejecutar.listaPends->cab;
	  printf("%s -> ", ret->cmd.ejecutar.fen);

	  while (cablista) {
	    printf("t%d", ((pID) cablista->elem)->id);
	    cablista = cablista->sig;
	    if (cablista) printf(" -> ");
	  }
	  printf("%s\n",ret->cmd.ejecutar.fsal);
	  break;
	}

      case CMD_ENVIARSENAL:
	{
	  pNodoLista cablista = ret->cmd.enviarsenal.listaEventos->cab;

	  while (cablista) {

	    pNodoLista pcablstprocs;
	    pEvento pevento = (pEvento) cablista->elem;

	    pcablstprocs = pevento->listaProgs->cab;
	    printf("evento: %d\n", pevento->evento);

	    while (pcablstprocs) {
	      printf("p%d\n", ((pID) pcablstprocs->elem)->id);
	      pcablstprocs = pcablstprocs->sig;
	    }

	    cablista = cablista->sig;
	    if (cablista) printf(" :: ");
	  }

	  break;
	}
      default:
	printf("digite un comando\n");
	break;
      }

      /* parser->free(parser); */
      /* tokens->free(tokens); */
      /* lexer->free(lexer); */
    }
  }
  return 0;
}
