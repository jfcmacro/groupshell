#include "GramAbsComandos.h"
#include <stdlib.h>
#include <string.h>

static pComandos
nuevoComandos() {
  pComandos ret;

  ret = (pComandos) malloc(sizeof(Comandos));

  return ret;
}

static void
borrarComando(pComandos cmd) {

  free(cmd);
}

pComandos
nuevoComandoCrear(char *prog, char* arg) {
  pComandos ret;

  ret = nuevoComandos();
  if (ret) {

    ret->etiqueta = CMD_CREAR;
    ret->cmd.crear.prog = prog;
    ret->cmd.crear.arg = arg;
  }

  return ret;
}

void
borrarComandoCrear(pComandos crear) {

  if (crear && crear->etiqueta == CMD_CREAR) {
    borrarCadena(crear->cmd.crear.prog);
    borrarCadena(crear->cmd.crear.arg);
    borrarComando(crear);
  }
}

pComandos
nuevoComandoDestruir(pLista listaProgs) {
  pComandos ret;

  ret = nuevoComandos();

  if (ret) {

    ret->etiqueta = CMD_DESTRUIR;
    ret->cmd.destruir.listaProgs = listaProgs;
  }

  return ret;
}

void
borrarComandoDestruir(pComandos destruir) {

  if (destruir && destruir->etiqueta == CMD_DESTRUIR) {

    borrarComando(destruir);
  }
}

pComandos
nuevoComandoListar() {

  pComandos ret;

  ret = nuevoComandos();

  if (ret) {

    ret->etiqueta = CMD_LISTAR;
    ret->cmd.listar.listaIds = NULL;
  }

  return ret;
}

pComandos
anadirListaAComandoListar(pComandos ret, pLista listaIds) {

  if (ret && ret->etiqueta == CMD_LISTAR) {

    ret->cmd.listar.listaIds = listaIds;
  }

  return ret;
}

void
borrarComandoListar(pComandos listar) {
  if (listar && listar->etiqueta == CMD_LISTAR) {
    borrarComando(listar);
  }
}

pComandos
nuevoComandoEjecutar(pLista listaPends, pCadena fen, pCadena fsal) {
  pComandos ret;

  ret = nuevoComandos();

  if (ret) {

    ret->etiqueta = CMD_EJECUTAR;
    ret->cmd.ejecutar.listaPends = listaPends;
    ret->cmd.ejecutar.fen  = fen;
    ret->cmd.ejecutar.fsal = fsal;
  }

  return ret;
}

void
borrarComandoEjecutar(pComandos ejecutar) {

  if (ejecutar && ejecutar->etiqueta == CMD_EJECUTAR) {
    borrarComando(ejecutar);
  }
}

pComandos
nuevoComandoBorrar(pLista listaPends) {

  pComandos ret;

  ret = nuevoComandos();

  if (ret) {

    ret->etiqueta = CMD_BORRAR;
    ret->cmd.ejecutar.listaPends = listaPends;
  }

  return ret;
}

void
borrarComandoBorrar(pComandos borrar) {

  if (borrar && borrar->etiqueta == CMD_BORRAR) {
    borrarComando(borrar);
  }
}

pComandos
nuevoComandoEnviarSenal(pLista listaEventos) {


  pComandos ret;

  ret = nuevoComandos();

  if (ret) {

    ret->etiqueta = CMD_ENVIARSENAL;
    ret->cmd.enviarsenal.listaEventos = listaEventos;
  }

  return ret;
}

void
borrarComandoEnviarSenal(pComandos enviarsenal) {

  if (enviarsenal
      && enviarsenal->etiqueta == CMD_ENVIARSENAL) {
    borrarComando(enviarsenal);
  }
}

pID
nuevoID(IDEtiqueta etiqueta, int id) {
  pID ret;
  ret = (pID) malloc(sizeof(ID));
  if (ret) {
    ret->etiqueta = etiqueta;
    ret->id = id;
  }
  return ret;
}

pID
nuevoIDPen(pANTLR3_STRING str) {

  return nuevoID(ID_PEN, atoi(((char *) str->chars) + 1));
}

pID
nuevoIDProg(pANTLR3_STRING str) {

  return nuevoID(ID_PROG, atoi(((char *) str->chars) + 1));
}

pEvento
nuevoEvento(int evento, pLista listaProgs) {
  pEvento ret;

  ret = (pEvento) malloc(sizeof(Evento));

  if (ret) {

    ret->evento = evento;
    ret->listaProgs = listaProgs;
  }

  return ret;
}
